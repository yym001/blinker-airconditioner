

#define BLINKER_WIFI
#define BLINKER_ESP_SMARTCONFIG 
#define IRPIN   3    //红外二极管引脚
#define DHTPIN  2   //DHT11引脚
#define DHTTYPE   DHT11 //DHT类型,我用的是DHT11

#define Slider_1 "ran-6i6" //温度滑动条
#define Slider_2 "ran-z8p" //风速滑动条

#define BUTTON_1 "btn-8bg" //电源按键
#define BUTTON_2 "btn-doa" //自动按键
#define BUTTON_3 "btn-gvv" //制冷按键
#define BUTTON_4 "btn-kop" //制热按键

#include <Blinker.h>
#include <ESP8266WiFi.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>  
#include <DHT.h>

//根据你的空调牌子更改头文件,头文件在你的Arduino安装目录下的 libraries ->  IRremoteESP8266 -> src
//例如你的是美的 的 就放 美的 的头文件
#include <ir_Coolix.h>     //这是美的在德国的一个牌子

IRCoolixAC Midea(IRPIN);  //实例化一个空调对象

BlinkerButton Button1(BUTTON_1);
BlinkerButton Button2(BUTTON_2);
BlinkerButton Button3(BUTTON_3);
BlinkerButton Button4(BUTTON_4);
BlinkerSlider Slider1(Slider_1);
BlinkerSlider Slider2(Slider_2);
BlinkerNumber HUMI("num-2yc");
BlinkerNumber TEMP("num-2cz");

DHT dht(DHTPIN, DHTTYPE);

char auth[] = "**************"; //你的设备密钥
float humi_read, temp_read;
uint32_t read_time = 0;
int32_t tem_value;
int32_t fan_value;
/* 电源按键按下时的回调函数 */
void button1_callback(const String & state)
{
    
    BLINKER_LOG("get button state: ", state);

      if (state == BLINKER_CMD_ON) {
        BLINKER_LOG("Toggle on!");
        Midea.on();
        Midea.send();
        Button1.color("#FF0000");
        Button1.print("on");
    }
   if (state == BLINKER_CMD_OFF) {
        BLINKER_LOG("Toggle off!");
        Midea.off();
        Midea.send();
        Button1.color("#000000");
        Button1.print("off");
    }
}

/* 自动按键按下时的回调函数 */
void button2_callback(const String & state)
{
  Midea.setMode(2);
  Midea.send();
  Button2.color("#FF0000");
  Button2.print();
  Button3.color("#000000");
  Button3.print();
  Button4.color("#000000");
  Button4.print();
}
/* 制冷按键按下时的回调函数 */
void button3_callback(const String & state)
{
  Midea.setMode(0);
  Midea.send();
  Button3.color("#0000FF");
  Button3.print();
  Button2.color("#000000");
  Button2.print();
  Button4.color("#000000");
  Button4.print();
}
/* 制热按键按下时的回调函数 */
void button4_callback(const String & state)
{
  Midea.setMode(3);
  Midea.send();
  Button4.color("#FF0000");
  Button4.print();
  Button2.color("#000000");
  Button2.print();
  Button3.color("#000000");
  Button3.print();

}


/* 滑动温度滑动条时 执行的函数 */
void slider1_callback(int32_t value)
{
    BLINKER_LOG("get slider value: ", value);
    tem_value = value;
    Midea.setTemp(value);
    Midea.send();
}
/* 滑动风速滑动条时 执行的函数 */
void slider2_callback(int32_t value)
{
    BLINKER_LOG("get slider value: ", value);
    fan_value = value;
    Midea.setFan(value-1);
    Midea.send();
}

void dataRead(const String & data)
{
    BLINKER_LOG("Blinker readString: ", data);

    Blinker.vibrate();
    
    uint32_t BlinkerTime = millis();
    
    Blinker.print("millis", BlinkerTime);
}

/*心跳包函数*/
void heartbeat()
{
  bool flag = Midea.getPower();
     if(flag) {  Button1.color("#FF0000"); Button1.print("on"); }
     else {Button1.color("#000000"); Button1.print("on");}
    HUMI.print(humi_read);
    TEMP.print(temp_read);
    Slider1.color("#FFD700");
    Slider1.print(tem_value);
    Slider2.color("#FFD700");
    Slider2.print(fan_value);
}

void setup() {
  Midea.begin();
  Serial.begin(115200);
  BLINKER_DEBUG.stream(Serial);
  pinMode(IRPIN,OUTPUT);
  pinMode(DHTPIN,INPUT);
  digitalWrite(IRPIN,LOW);
   
  Blinker.begin(auth);
  Blinker.attachData(dataRead);
  Blinker.attachHeartbeat(heartbeat);
  Button1.attach(button1_callback);
  Button2.attach(button2_callback);
  Button3.attach(button3_callback);
  Button4.attach(button4_callback);
  
  
  Slider1.attach(slider1_callback);
  Slider2.attach(slider2_callback);
}


void loop() {
  
  Blinker.run();
      if (read_time == 0 || (millis() - read_time) >= 2000)
    {
        read_time = millis();

        float h = dht.readHumidity();
        float t = dht.readTemperature();        

        if (isnan(h) || isnan(t)) {
            BLINKER_LOG("Failed to read from DHT sensor!");
            return;
        }

        float hic = dht.computeHeatIndex(t, h, false);

        humi_read = h;
        temp_read = t;
    }
  
}
